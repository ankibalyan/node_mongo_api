const RecordModel = require('../models');

async function fetchData(req, res) {
  const { startDate, endDate, minCount = 0, maxCount = Infinity } = req.body;

  const options = {};

  if (startDate && endDate) {
    try {
      options.createdAt = { $gte: new Date(startDate), $lte: new Date(endDate) };
    } catch (error) {
      console.log('Invalid date error');
    }
  }

  const data = await RecordModel.find(options);

  const records = data
    .map((r) => {
      let totalCount = 0;
      if (Array.isArray(r.counts)) {
        totalCount = r.counts.reduce((tc, currValue) => {
          return tc + currValue;
        }, 0);
      }

      return {
        key: r.key,
        createdAt: r.createdAt,
        totalCount,
      };
    })
    .filter((r) => r.totalCount >= minCount && r.totalCount <= maxCount);

  return res.send({
    code: 0,
    msg: 'Success',
    records,
  });
}

module.exports = {
  fetchData,
};
