const mongoose = require('mongoose');
const MONGO_URL = process.env.MONGO_URL;

if (!MONGO_URL) {
  throw new Error('Please define the MONGO_URL environment variable inside .env');
}

main().catch((err) => console.log(err));

async function main() {
  await mongoose.connect(MONGO_URL);
}
