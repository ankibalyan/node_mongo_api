const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const RecordSchema = new Schema({}, { strict: false });

RecordSchema.set('toJSON', {
  virtuals: true,
  versionKey: false,
  transform: function (doc, ret) {
    delete ret._id;
  },
});

module.exports = mongoose.model('Record', RecordSchema, 'records');
