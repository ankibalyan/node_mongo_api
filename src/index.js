require('dotenv').config();
const createError = require('http-errors');
const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const helmet = require('helmet');
require('./db/mongo');

const indexRouter = require('./routes/index');

const app = express();

app.disable('x-powered-by');
app.use(helmet());
app.use(logger('dev'));
app.use(express.json({ extended: true }));
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());

app.use('/favicon.ico', express.static('images/favicon.ico'));
app.use('/', indexRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res) {
  // set error status
  res.status(err.status || 500);
});

module.exports = app;
