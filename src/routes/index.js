const express = require('express');
const { fetchData } = require('../controllers');

const router = express.Router();

router.post('/', fetchData);

module.exports = router;
